"""Generate each message dynamicaly."""
# fmt: off
from dataclasses import dataclass
mess = {"win":    "YOU WON !!!",
        "wrong":  "big ass number try again",
        "over":   "GAME OVER!! \nRESTARTING"}

colors = {"warn": "red",
          "win": "red",
          "wrong": "red"}

lifes: int = 3


@dataclass
class Message:
    """A class to store the options for the message that will be desplayed."""
    text: str
    color: str
    font: tuple = ('Hack', 20, 'bold', 'italic')


class Generator:
    """Generates the messages for the user."""

    def _populate_message_fields(self, chosen_text: str, col: str) -> Message:
        my_message = Message(chosen_text, col)
        return my_message

    def win_mess_opt(self) -> Message:
        """Create the winning message."""
        win_mess = self._populate_message_fields(
            mess['win'], col=colors["win"])
        return win_mess
