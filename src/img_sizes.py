#!/usr/bin/env python
from pathlib import Path
from PIL import Image
from chop_image import the_file_names


def select_image_to_play(which: int) -> tuple:
    """Handle multiple images for multiple rounds of the game."""

    if which <= len(the_file_names):
        foto_name = the_file_names[which]
        file_path_cutted = Path(f"cutted_auto/{foto_name}/{foto_name}_0.png")
        cutted_image = Image.open(file_path_cutted)
        # original_image = Path(f"assets/{foto_name}.png")
        # original_image = Image.open(ORIGINAL_IMAGE)
        return file_path_cutted, cutted_image
    print("you dont have so many images")
    return (-1, -1)
