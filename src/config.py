"""Constant values and aliases for types."""
# fmt:off
import tkinter as tk
from typing import List

from tkthread import TkThread

from gen_popers import Generator


root = tk.Tk()
tkt = TkThread(root)
tkt.setDaemon(True)
root.geometry("1080x700")
root['background'] = "#3F3F3F"


class UiItems:
    """Handle Ui elements from tkinter library."""
    def __init__(self) -> None:
        self.winning: tk.Label
        self.mess_wrapper: Generator = Generator()

    def win_label(self, fr_lbl: tk.Frame) -> tk.Label:
        """Create the label to prompt the user that he won."""

        win_mess = self.mess_wrapper.win_mess_opt()
        self.winning = tk.Label(fr_lbl,
                                text=win_mess.text,
                                highlightcolor=win_mess.color,
                                foreground=win_mess.color,
                                font=win_mess.font)
        return self.winning


def remove_label(label: tk.Label) -> None:
    """Destroy message label when it is needed."""
    label.destroy()


def remove_all_labels(labels: List[tk.Label]) -> None:
    """Destroy all image labels when complete."""
    for let_me_name_this in labels:
        remove_label(let_me_name_this)
