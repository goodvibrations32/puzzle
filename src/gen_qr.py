"""Create the pairing qrs as images."""
import os
import shutil
from pathlib import Path
from typing import List
import qrcode

from chop_image import ROWS, COLUMNS

data: List[int] = []

# create seperate folder to store the qr codes for decoding...
# they can be used with any cutted image if the number of qrs are
# equal to the number of sliced pieces of the image...
qr_folder = Path("qrs_as_img/")
if not qr_folder.exists():
    os.mkdir(qr_folder)
else:
    shutil.rmtree(qr_folder)
    os.mkdir(qr_folder)

for item in range(ROWS * COLUMNS):
    data.append(item)

for elem in data:
    img = qrcode.make(elem)
    img.save(f"{qr_folder}/qr_{data[elem]}.png")
# print(data)
