"""Here the state of the game is recorded for control flow."""
from typing import List, Dict


class Tracker():
    """An object to track the state and the assets of the Game."""
    def __init__(self) -> None:
        self.past_inputs: List[int] = []
        self.cutted_image_list: List[str] = []
        self.key_pairs: Dict[int, str] = {}

    def load_images_in_mem(self, image_name: str) -> List[str]:
        """Return the list of names of the cutted images.
        Args:
            image_name:The original image name which was used to cut in
              piecies. Pass the foo.png as image_name = "foo"
              the arguments.
            image_id:Create a number identifier for each image which already
                   is labeled with foo_x.png.

        Returns:
                self.cutted_image_list: A list of strings with the name of the
                images with the prefix "./cutted/" to indicate that the
                processed image will be in a separate folder in the root
                project.
        """
        self.cutted_image_list.append(
            f"{image_name}")
        return self.cutted_image_list

    def create_pairing_qrs(self) -> Dict:
        """Associate each image with an integer for identification.

        Args:
            self: Using this function will create a dictionary and store the
                images with a separate id in order to acossiate each image with
                a generic id.

        Returns:
               self.key_pairs: A dictionary of strings with the name of the
                    images with the prefix "./cutted/" to indicate that the
                    processed image will be in a separate folder in the root
                    project.
        """

        for i, each in enumerate(self.cutted_image_list):
            self.key_pairs[i] = each
        return self.key_pairs

    def record_inputs(self, the_last: int) -> List[int]:
        """Keep a record of images founded by the user.
        Args:
            the_last: The last input given by the user to be appended in the
                recording list of inputs.

        Returns:
               self.past_inputs: A list of integers for storing the past
                    inputs.
        """

        self.past_inputs.append(the_last)
        return self.past_inputs
