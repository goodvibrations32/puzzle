"""The main file to run the hole shit"""
# fmt: off

from config import tkt, root
from game import game_loop
from read_qr import Reader


if __name__ == "__main__":

    tkt.nosync(game_loop)
    tkt.nosync(Reader.show_frame)
    root.mainloop()
