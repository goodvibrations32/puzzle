"""Here all the game loop and flow is managed."""
# import time
import tkinter as tk
from typing import Any, List

from PIL import Image, ImageTk

from config import (UiItems,
                    remove_all_labels, remove_label)
from img_sizes import select_image_to_play
from read_qr import Reader
from track_state import Tracker


def game_loop() -> Any:
    """Here is all the game logic."""
    game_round = 0
    file_path_cutted, cutted_im = select_image_to_play(game_round)
    # The game frame
    frame1 = tk.Frame()
    frame1.grid(row=4, column=5)
    frame1['background'] = "#819B69"
    # This makes the grid cells to have a minimum size!! add 2 for 4 bord/dth
    frame1.grid_rowconfigure(tuple(range(4)), minsize=cutted_im.size[1] + 2)
    frame1.grid_columnconfigure(tuple(range(5)), minsize=cutted_im.size[0] + 2)

    tracker = Tracker()
    for item in range(20):
        pieces_names = str(file_path_cutted).replace("_0", f"_{item}")
        tracker.load_images_in_mem(f"{pieces_names}")

    tracker.create_pairing_qrs()
    tk_lst = [ImageTk.PhotoImage(Image.open(x)
                                 ) for x in tracker.key_pairs.values()]
    lbl_list: List[tk.Label] = []

    loop_controller = 0
    qr_read = Reader()
    while loop_controller < 20:
        my_in = qr_read.show_frame()

        # Handle the images for the first row
        if my_in <= 4 and my_in not in tracker.past_inputs:
            lbl = tk.Label(frame1, image=tk_lst[my_in], background='#3F3F3F')
            lbl.grid(row=0, column=my_in, columnspan=1)
            tracker.record_inputs(the_last=my_in)
            lbl_list.append(lbl)

        # Handle the images for the second row
        elif my_in <= 9 and my_in not in tracker.past_inputs:
            lbl = tk.Label(frame1, image=tk_lst[my_in], background='#3F3F3F')
            lbl.grid(row=1, column=my_in - 5, columnspan=1)
            tracker.record_inputs(the_last=my_in)
            lbl_list.append(lbl)

        # Handle the images for the third row
        elif my_in <= 14 and my_in not in tracker.past_inputs:
            lbl = tk.Label(frame1, image=tk_lst[my_in], background='#3F3F3F')
            lbl.grid(row=2, column=my_in - 10, columnspan=1)
            tracker.record_inputs(the_last=my_in)
            lbl_list.append(lbl)

        # Handle the images for the fourth row
        elif my_in <= 19 and my_in not in tracker.past_inputs:
            lbl = tk.Label(frame1, image=tk_lst[my_in], background='#3F3F3F')
            lbl.grid(row=3, column=my_in - 15, columnspan=1)
            tracker.record_inputs(the_last=my_in)
            lbl_list.append(lbl)

        # End game state jobs
        if (len(tracker.past_inputs) == len(tracker.cutted_image_list) and (
                my_in <= 19)):
            winner = UiItems().win_label(frame1)
            frame1.after(50, winner.place(x=350, y=350))

            # handle the clearing of the board
            frame1.after(5000, lambda: remove_label(winner))
            frame1.after(7000, lambda: remove_all_labels(lbl_list))
            # frame1.grid_columnconfigure(tuple(range(5)), weight=0)

            tracker.past_inputs = []
            loop_controller = 0
            game_round += 1
            tracker = Tracker()
            # time.sleep(5)
            file_path_cutted, cutted_im = select_image_to_play(game_round)
            frame1.after(8000,
                         lambda: frame1.grid_rowconfigure(tuple(range(4)),
                                                          minsize=cutted_im.size[1] + 2))

            frame1.after(8000,
                         lambda: frame1.grid_columnconfigure(tuple(range(5)),
                                                             minsize=cutted_im.size[0] + 2))
            for item in range(20):
                pieces_names = str(file_path_cutted).replace("_0", f"_{item}")
                tracker.load_images_in_mem(f"{pieces_names}")

            tracker.create_pairing_qrs()
            tk_lst = [ImageTk.PhotoImage(Image.open(x)
                                         ) for x in tracker.key_pairs.values()]
            # lbl_list: List[tk.Label] = []
