#!/usr/bin/env ipython
from tkinter import filedialog as fd

filetypes_supported = [("Images", ".png")]

give_me_names = fd.askopenfilenames(title="select the images to cut",
                                    initialdir="assets/",
                                    filetypes=filetypes_supported)
# print(give_me_names)
