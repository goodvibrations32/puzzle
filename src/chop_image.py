"""Slicing the images in a separate folder."""
import os
import shutil
import fnmatch
from pathlib import Path
from split_image import split_image

# this import is needed if we want to make this manually as the TODO bellow
from ask_filez import give_me_names

DEFIM_PFX = 'assets'
DEFCUT_PFX = 'cutted_auto'

ROWS = 4
COLUMNS = 5

for name in os.listdir(f'{DEFIM_PFX}'):
    # support only png's
    if fnmatch.fnmatch(name, "*.png"):
        ORIGINAL_FILE = name
    else:
        continue

    # Move one dir up because when i call split_image() we are already in
    # the folder that the chopped image will be dumped so 1 level down from
    # /project_root/
    pth_img_to_split = Path(f"{DEFIM_PFX}/{ORIGINAL_FILE}")

    # DONE this is to make the selection from a gui file manager and let
    # the user put each image manualy
    the_file_names = []
    for each in give_me_names:
        user_image = os.path.splitext(each)[0]
        file_name = os.path.basename(os.path.normpath(user_image))
        the_file_names.append(file_name)

    # subdir_name_from_image = os.path.splitext(ORIGINAL_FILE)[0]

# if __name__ == '__main__':

#     # create a directory to cut the image cleanly and if exists remove it
#     # to create with the new image
#     pth_for_slices = Path(f"{DEFCUT_PFX}/{subdir_name_from_image}")
#     if not pth_for_slices.exists():
#         os.mkdir(pth_for_slices)
#     else:
#         shutil.rmtree(pth_for_slices)
#         os.mkdir(pth_for_slices)

#     split_image(pth_img_to_split, ROWS, COLUMNS,
#                 False, False, False,
#                 pth_for_slices)
