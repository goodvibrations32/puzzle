"""Read qrcodes with openCv and a camera."""
import sys
from dataclasses import dataclass
from typing import Any
import tkinter as tk

import cv2
import pyzbar.pyzbar as pyzbar
from PIL import Image, ImageTk

from config import root

# Video window
imageFrame = tk.Frame(root, width=50, height=80)
imageFrame.grid(row=0, column=0, padx=5, pady=5)

# Capture video frames
lmain = tk.Label(imageFrame, borderwidth=4, relief="solid")
lmain.grid(row=0, column=0)

qcd = cv2.QRCodeDetector()
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 256)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 256)


@dataclass
class Reader:
    """Here we handle all opencv stuff.
    We capture the recorded decoded text from the qr.
    """
    my_data: int = sys.maxsize  # sys.maxsize

    @classmethod
    def __decoding(cls, camera: Any) -> tuple[
            int, Any]:
        _, frame = camera.read()
        decoded = pyzbar.decode(frame)
        for obj in decoded:
            if obj != []:
                cls.my_data = int(obj.data)
                print(f"dec: {cls.my_data}")
                break

            # release the camera to redetect in Runtime
            camera.release()

        return cls.my_data, frame

    @classmethod
    def show_frame(cls) -> int:
        """Detect and decode the QR image.
        For now all we take is a number.
        """

        past = cls.my_data
        _, frm = Reader.__decoding(camera=cap)
        cv2image = cv2.cvtColor(frm, cv2.COLOR_BGR2RGBA)
        img = Image.fromarray(cv2image)
        imgtk = ImageTk.PhotoImage(image=img)
        lmain.configure(image=imgtk)
        new = cls.my_data

        if past == new:
            lmain.update()
            # return something that is not used as an id internaly (i.e. 0..20)
            return sys.maxsize

        print(f"new: {new}")
        return cls.my_data
