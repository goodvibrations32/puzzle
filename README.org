#+title: A "jigsaw" puzzle game from images
* The idea
Create an interactive game with images by cutting them in a grid layout and associate each one with some physical object for the player to find.
* Demo
Colors are nice and functionality of the game is pretty much ready.
#+ATTR_HTML: width="250px"
[[file:examples/avatar.png]]

[[./examples/video_puzzle_qr_example.mp4][Video]]

#+begin_notes
+add a video here to showcase+
#+end_notes
* Roadmap
- [X] repeat until a point
- [X] adding a replay and life span for the game
- [X] leave the tiles that are not found with spaces (we can tweak for braking lines grid...)
- [X] use QR Codes as the player input
- [-] using multiple image size concise
  - [-] test big images
  - [ ] test details on the borders of an image (frame styles etc..)
- [-] cut images from a given folder
  - [X] cut an image
  - [ ] cut multiple from folder
  - [ ] dump each cutted to a separate folder
- [X] figure out sizes dynamically from user defined images
- [ ] organize them in separate folders

* Credits
The image selected for the testing originates from a person very enspiring and strong; Kris Nova's banner foto. R.I.P. August '23.
